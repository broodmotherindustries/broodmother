from cmd import Cmd
from Shared import EventHook
import sys
import os
from Shared import plugin_manager
from Shared import Diagnostic
from Shared import SystemUtls

class MyPrompt(Cmd):
    connected_bot = None

    prompt = 'pb> '
    intro = "Welcome! Type ? to list commands"
    events = EventHook()

    def __init__(self, completekey='tab', stdin=sys.stdin, stdout=sys.stdout):
        self.systemUtils = SystemUtls()
        self.plugin_manager = plugin_manager.PluginManager(plugin_folder='./plugins')
        self.on_get_bots = EventHook()
        self.on_get_bot = EventHook()
        self.on_exit = EventHook()
        self.on_connect_bot = EventHook()
        self.on_disconnect_from_bot = EventHook()
        self.on_any = EventHook()
        self.on_get_bot_help = EventHook()
        self.completekey = completekey
        self.stdin = stdin
        self.stdout = stdout
        self.cmdqueue = []
        for cmd in self.plugin_manager.get_available_plugins():
            self.plugin_manager.load_plugin(cmd)
            self.func_builder(cmd)
            setattr(self.__class__, 'help_'+cmd,  self.func_builder(cmd))

    def func_builder(self, name):
        def f(self):
            print(self.plugin_manager.help_hook(name))
        return f

    def do_exit(self, inp):
        self.on_disconnect_from_bot.fire()

    def help_exit(self):
        print('exit the application. Shorthand: x q Ctrl-D.')

    def do_get_bots(self, inp):
        self.on_get_bots.fire()

    def do_get_bot(self, inp):
        self.on_get_bot.fire(inp)

    def help_get_bot(self):
        print ('returns details about the selected bot')

    def help_get_bots(self):
        print ('returns all the bots connected to the system')

    def do_connect_bot(self, inp):
        self.on_connect_bot.fire(inp)

    def do_connect(self, inp):
        self.on_connect_bot.fire(inp)

    def do_diagnostic(self, inp):
        print("RAM = " + str(self.systemUtils.getRAMinfo()) + "%")
        print("CPU usage = " + str(self.systemUtils.getCPUuse()) + "%")
        print(self.systemUtils.getDiskSpace())
        dia = Diagnostic(test_folder='./tests')
        dia.load_all_test()
        print(dia.run_all_unit_tests())

    def help_connect_bot(self):
        print('connects to the bot')

    def help_connect(self):
        print('connects to the bot')

    def do_disconnect(self):
        self.on_disconnect_from_bot.fire()

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)
        self.on_any.fire(inp)

    def do_help(self, arg):
        if self.connected_bot is None:
            Cmd.do_help(self, arg)
        else:
            self.on_get_bot_help.fire()
            Cmd.do_help(self, arg)

    def show_help(self, arg):
        Cmd.do_help(self, arg)

    def do_shell(self, s):
        os.system(s)

    def help_shell(self):
        print ("execute shell commands")

    def can_exit(self):
        return True

    def onecmd(self, line):
        r = super(MyPrompt, self).onecmd(line)
        if r and (self.can_exit() or
                  input('exit anyway ? (yes/no):') == 'yes'):
            return True
        return False

    do_EOF = do_exit
    help_EOF = help_exit


if __name__ == '__main__':
    MyPrompt().cmdloop()
