from flask import Flask
from flask import request
from flask_socketio import SocketIO, Namespace
from Shared import EventHook
import logging


class Websocket:

    def __init__(self, port=5000):
        logging.getLogger('socketio').setLevel(logging.ERROR)
        logging.getLogger('engineio').setLevel(logging.ERROR)
        self.onMessage = EventHook()
        self.on_connect = EventHook()
        self.on_new_bot = EventHook()
        self.on_command_result = EventHook()
        self.port = port
        self.app = Flask('')
        self.app.config['SECRET_KEY'] = 'secret!'
        self.socket_io = SocketIO(self.app)
        self.socket_io.on_event('NEW BOT', self.handle_new_bot)
        self.socket_io.on_event('UPDATE BOT', self.handle_update_bot)
        self.socket_io.on_event('command_results', self.handle_command_results)
        # self.socket_io.on('connect', self.handle_connect)
        self.socket_io.on_event('connect', self.handle_connect)
        self.socket_io.on_event('disconnect', self.handle_connect)
        self.simulatorCommands = SimulatorNamespace()
        self.simulatorCommands.on_simulator_connect += self.sim_connected
        self.simulatorCommands.on_simulator_update_bots += self.simulatorCommands.on_simulator_update_bots
        self.socket_io.on_event('connect', self.simulatorCommands.on_connect, '/simulator')

    def run(self):
        try:
            self.socket_io.run(self.app, port=self.port)
        except Exception as e:
            print(e)

    def sim_connected(self):
        print("simulator connected")


    def stop(self):
        self.socket_io.stop()

    def handle_message(self, message):
        self.onMessage.fire(message)

    def send_message(self, message):
        self.socket_io.send(message)

    def handle_connect(self):
        self.on_connect.fire(request.sid)

    def handle_disconnect(self):
        self.on_connect.fire(request.sid)

    def handle_new_bot(self, json):
        self.on_new_bot.fire(json, request.sid)

    def handle_update_bot(self, json):
        self.socket_io.emit('updateBot', json)

    def emit_message(self, command, data, sid):
        self.socket_io.emit(command, data, room=sid)

    def handle_command_results(self, json):
        # TODO: added code to deal with the results.
        self.on_command_result.fire(json)
        pass


class SimulatorNamespace:

    def __init__(self):
        self.on_simulator_connect = EventHook()
        self.on_simulator_disconnect = EventHook()
        self.on_simulator_update_bots = EventHook()

    def on_connect(self):
        self.on_simulator_connect.fire()
        pass

    def on_disconnect(self):
        self.on_simulator_disconnect.fire()
        pass

    def on_updateBot(self, bots):
        pass

    def on_my_event(self, data):

        pass
