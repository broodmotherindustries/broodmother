
import uuid
import re
import unittest
import xmlrunner
from unittest.mock import patch
import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)


class TestMain(unittest.TestCase):
    def setUp(self):
        pass

    def test_true(self):
        self.assertEqual(True, True)

if __name__ == '__main__':
    unittest.main(
            testRunner=xmlrunner.XMLTestRunner(open('../report.xml', 'w'), outsuffix = ''),
            failfast=False,
            buffer=False,
            catchbreak=False)